from app import db, app, oauth , database
from app.forms import LoginForm, RegisterForm
from app.models import User, weatherFLWTC
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required, current_user


from API.HKOAPI.FLW.flw import stats, GS, TI, FDW, FP, FD, O, UT
from flask import flash, render_template, redirect, url_for, request, session , Flask
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
from API.HKOAPI.FLW.flw import stats, GS, TI, FDW, FP, FD, O, UT


from app import app


from .database.db import create_db, create_user, get_all_users, set_user_profile_picture_file_names
from .uploads.file_handler import is_file_type_allowed, upload_file_to_s3
import os
from werkzeug.utils import secure_filename 

##
from .utils.Create_Collection import list_collections,create,delete
from .utils.Register_Faces import add_face_to_collection
from .utils.Face_recognize import face_recognition_saving_image



mysql = MySQL(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"



@app.route('/explore')
def explore():
    return render_template('explore.html')



@app.route('/test')
def test1():
    return render_template('test.html')

@app.route('/hiking1')
def hiking1():
    return render_template('hiking1.html')

@app.route('/main')
def test():
    return render_template('main.html')

@app.route('/login-page', methods=['GET','POST'])
def login_page():
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password1.data):
            flash('Invalid username or password')
            return redirect(url_for('login_page'))
        login_user(user)
        return redirect(url_for('dashboard'))
    return render_template('login-page.html', form=form)

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/dashboard', methods=['GET','POST'])
@login_required
def dashboard():
    googlename = dict(session).get('email', None)
    GSit = db.session.query(weatherFLWTC.generalSituation).order_by(weatherFLWTC.id.desc()).first()
    UTime = db.session.query(weatherFLWTC.updateTime).order_by(weatherFLWTC.id.desc()).first()
    return render_template('dashboard.html', GSit=GSit, UTime=UTime, googlename=googlename)

@app.route('/navbar')
def navbar():
    return render_template('navbar.html')

@app.route('/test1')
def navbar2():
    return render_template('navbar1.html')

@app.route('/test-form')
def test_form():
    form = RegisterForm()
    return render_template('test_register.html', form=form)

@app.route('/googlelogin')
def googlelogin():
    google = oauth.create_client('google')
    redirect_uri = url_for('googleauthorize', _external=True)
    return google.authorize_redirect(redirect_uri)

@app.route('/googleauthorize')
def googleauthorize():
    google = oauth.create_client('google')
    token = google.authorize_access_token()
    resp = google.get('userinfo')
    resp.raise_for_status()
    user_info = resp.json()
    # do something with the token and profile
    session['email'] = user_info['email']
    return redirect('/')

@app.route('/googlelogout')
def googlelogout():
    for key in list(session.keys()):
        session.pop(key)
    return redirect('/')


 #test mysql 
@app.route('/')
@app.route('/login', methods =['GET', 'POST'])
def login():
    msg = ''
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE username = % s AND password = % s', (username, password, ))
        account = cursor.fetchone()
        if account:
            session['loggedin'] = True
            session['id'] = account['id']
            session['username'] = account['username']
            msg = 'Logged in successfully !'
            return render_template('index.html', msg = msg)
        else:
            msg = 'Incorrect username / password !'
    return render_template('login.html', msg = msg)
 
@app.route('/logout')
def logout():
   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('username', None)
   return redirect(url_for('login'))
 
@app.route('/register', methods =['GET', 'POST'])
def register():
    msg = ''
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form and 'address' in request.form and 'city' in request.form and 'country' in request.form and 'postalcode' in request.form and 'organisation' in request.form:
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        organisation = request.form['organisation'] 
        address = request.form['address']
        city = request.form['city']
        state = request.form['state']
        country = request.form['country']   
        postalcode = request.form['postalcode']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE username = % s', (username, ))
        account = cursor.fetchone()
        if account:
            msg = 'Account already exists !'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Invalid email address !'
        elif not re.match(r'[A-Za-z0-9]+', username):
            msg = 'name must contain only characters and numbers !'
        else:
            cursor.execute('INSERT INTO accounts VALUES (NULL, % s, % s, % s, % s, % s, % s, % s, % s, % s)', (username, password, email, organisation, address, city, state, country, postalcode, ))
            mysql.connection.commit()
            msg = 'You have successfully registered !'
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template('register.html', msg = msg)
 
 
@app.route("/index")
def index():
    if 'loggedin' in session:
        return render_template("index.html")
    return redirect(url_for('login'))
 
 
@app.route("/display")
def display():
    if 'loggedin' in session:
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE id = % s', (session['id'], ))
        account = cursor.fetchone()   
        return render_template("display.html", account = account)
    return redirect(url_for('login'))
 
@app.route("/update", methods =['GET', 'POST'])
def update():
    msg = ''
    if 'loggedin' in session:
        if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form and 'address' in request.form and 'city' in request.form and 'country' in request.form and 'postalcode' in request.form and 'organisation' in request.form:
            username = request.form['username']
            password = request.form['password']
            email = request.form['email']
            organisation = request.form['organisation'] 
            address = request.form['address']
            city = request.form['city']
            state = request.form['state']
            country = request.form['country']   
            postalcode = request.form['postalcode']
            cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute('SELECT * FROM accounts WHERE username = % s', (username, ))
            account = cursor.fetchone()
            if account:
                msg = 'Account already exists !'
            elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
                msg = 'Invalid email address !'
            elif not re.match(r'[A-Za-z0-9]+', username):
                msg = 'name must contain only characters and numbers !'
            else:
                cursor.execute('UPDATE accounts SET  username =% s, password =% s, email =% s, organisation =% s, address =% s, city =% s, state =% s, country =% s, postalcode =% s WHERE id =% s', (username, password, email, organisation, address, city, state, country, postalcode, (session['id'], ), ))
                mysql.connection.commit()
                msg = 'You have successfully updated !'
        elif request.method == 'POST':
            msg = 'Please fill out the form !'
        return render_template("update.html", msg = msg)
    return redirect(url_for('login'))
 


@app.route('/mainpage', methods=["POST", "GET"])
def indexorginial():    # put application's code here
    r = stats()
    if r == 200:
        apical_to_create = weatherFLWTC(generalSituation=GS(),
                                        tcInfo=TI(),
                                        fireDangerWarning=FDW(),
                                        forecastPeriod=FP(),
                                        forecastDesc=FD(),
                                        outlook=O(),
                                        updateTime=UT())
        db.session.add(apical_to_create)
        db.session.commit()
    GSit = db.session.query(weatherFLWTC.generalSituation).order_by(weatherFLWTC.id.desc()).first()
    UTime = db.session.query(weatherFLWTC.updateTime).order_by(weatherFLWTC.id.desc()).first()
    return render_template('index-original.html', GSit=GSit, UTime=UTime)

@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404

@app.errorhandler(500)
def page_not_found(e):
    return render_template("500.html"), 500


@app.route('/registerorginial', methods=['GET','POST'])
def register_page():
    form = RegisterForm()
    if form.validate_on_submit():
        user_to_create = User(username=form.username.data,
                              email_address=form.email_address.data)
        user_to_create.set_password(form.password1.data)
        db.session.add(user_to_create)
        db.session.commit()
        return redirect(url_for('index'))
    if form.errors != {}: #if there are not errors form the validation
        for err_msg in form.errors.values():
            flash(f'There was an error with creating a user:{err_msg}',category='danger')
    return render_template('register.html', form=form)


@app.route('/logoutorginial', methods=['GET', 'POST'])
@login_required
def logoutorginial():
    logout_user()
    return 'You are now logged out!'


## aws reko 


@app.route("/rekohome", methods=['GET'])
def home():    
    users = get_all_users()
    return render_template('s3page.html', users=users)


@app.route("/sign-up-new-user", methods=['POST'])
def sign_up_new_user():
    name = request.form['name']
    create_user(name)
    return redirect(url_for('s3page.html'))


@app.route("/upload-image/<user_id>", methods=['POST'])
def upload_image(user_id):
    if 'file' not in request.files:
        flash('No file uploaded', 'danger')
        return redirect(url_for('s3page.html'))
    
    file_to_upload = request.files['file']

    if file_to_upload.filename == '':
        flash('No file uploaded', 'danger')
        return redirect(url_for('s3page'))
    
    if file_to_upload and is_file_type_allowed(file_to_upload.filename):
        provided_file_name = secure_filename(file_to_upload.filename)
        stored_file_name = upload_file_to_s3(file_to_upload, provided_file_name)
        set_user_profile_picture_file_names(user_id, stored_file_name, provided_file_name)
        flash(f'{provided_file_name} was successfully uploaded', 'success')
    
    return redirect(url_for('s3page.html'))


### 
@app.route('/reko')
def start_page():
    return render_template('reko_index.html')

@app.route('/collection_page')
def collection_page():
    count,lst=list_collections()
    return render_template('collection.html', count=count, lst=lst)


@app.route('/create_page', methods=['POST'])
def create_page():
    COLLECTION_NAME = str(request.form['collection-name'])
    COLLECTION_NAME=COLLECTION_NAME.strip()
    print(COLLECTION_NAME)
    statement=create(COLLECTION_NAME)
    print(statement)
    count,lst=list_collections()
    return render_template('collection.html', count=count, lst=lst, statement=statement)


@app.route('/delete_page')
def delete_page():
    COLLECTION_NAME=request.args.get('name')
    statement=delete(COLLECTION_NAME)
    count,lst=list_collections()
    return render_template('collection.html', count=count, lst=lst, statement=statement)


@app.route('/register_page_reko')
def register_page_reko():
    count,lst=list_collections()
    return render_template('register_reko.html', lst=lst)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/register_faces', methods=['POST'])
def register_faces():
    if 'file' not in request.files:
        statement='No file part'
        count, lst = list_collections()
        return render_template('register_reko.html', lst=lst,statement=statement)
    file = request.files['file']
    if file.filename == '':
        statement='No image selected for uploading'
        count, lst = list_collections()
        return render_template('register.html', lst=lst, statement=statement)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        Register_image = Image.open('static/uploads/' + filename)
        print(Register_image)
        bytes_array = io.BytesIO()
        Register_image.save(bytes_array, format="PNG")
        source_image_bytes = bytes_array.getvalue()
        name = str(request.form['person-name'])
        name = name.strip()
        COLLECTION_NAME=request.form['collection']
        print(name)
        print(COLLECTION_NAME)
        ount, lst = list_collections()
        registration_result = add_face_to_collection(source_image_bytes, name, COLLECTION_NAME)
        #print('upload_image filename: ' + filename)
        # flash('Image successfully uploaded and displayed below')
        return render_template('register.html', lst=lst, reg_lst=registration_result, filename=filename)
    else:
        statement='Allowed image types are -> png, jpg, jpeg, gif'
        count, lst = list_collections()
        return render_template('reko_register.html', lst=lst, statement=statement)

@app.route('/display/<filename>')
def display_image(filename):
    #print('display_image filename: ' + filename)
    return redirect(url_for('static', filename='uploads/' + filename), code=301)

@app.route('/display/<filename>')
def display_image_recognition(filename):
    #print('display_image filename: ' + filename)
    return redirect(url_for('static', filename='result/' + filename), code=301)

@app.route('/recognize_page')
def recognize_page():
    count,lst=list_collections()
    return render_template('recognize.html', lst=lst)

@app.route('/recognize_faces', methods=['POST'])
def recognize_faces():
    if 'file' not in request.files:
        statement='No file part'
        count, lst = list_collections()
        return render_template('recognize.html', lst=lst,statement=statement)
    file = request.files['file']
    if file.filename == '':
        statement='No image selected for uploading'
        count, lst = list_collections()
        return render_template('recognize.html', lst=lst, statement=statement)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        Register_image = Image.open('static/uploads/' + filename)
        print(Register_image)

        COLLECTION_NAME=request.form['collection']
        print(COLLECTION_NAME)
        count, lst = list_collections()

        path="result/"+filename
        result_img,res_lst = face_recognition_saving_image(Register_image, COLLECTION_NAME)
        result_img.save('static/'+path)

        #print('upload_image filename: ' + filename)
        # flash('Image successfully uploaded and displayed below')
        return render_template('recognize.html', lst=lst, filename=path,res_lst=res_lst)
    else:
        statement='Allowed image types are -> png, jpg, jpeg, gif'
        count, lst = list_collections()
        return render_template('recognize.html', lst=lst, statement=statement)

@app.after_request
def add_header(response):
    response.headers['Pragma'] = 'no-cache'
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Expires'] = '0'
    return response

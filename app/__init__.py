from flask import Flask, render_template, request, redirect, url_for, flash, session
from flask_mysqldb import MySQL

##
from .database.db import create_db, create_user, get_all_users, set_user_profile_picture_file_names
from .uploads.file_handler import is_file_type_allowed, upload_file_to_s3
import os
from werkzeug.utils import secure_filename 

##
from .utils.Create_Collection import list_collections,create,delete
from .utils.Register_Faces import add_face_to_collection
from .utils.Face_recognize import face_recognition_saving_image

##
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from authlib.integrations.flask_client import OAuth

import MySQLdb.cursors
import re
 



from PIL import Image
UPLOAD_FOLDER = 'static/uploads/'


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + 'app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = '220f867f35c3854d9923ee22'

app.secret_key = '220f867f35c3854d9923ee22'
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'P@ssw0rd1'
app.config['MYSQL_DB'] = 'hiking_app_cheerful'

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])



database = create_db()

db = SQLAlchemy(app)
migrate = Migrate(app, db)
 


oauth = OAuth(app)
google = oauth.register(
    name='google',
    client_id='',
    client_secret='',
    access_token_url='https://accounts.google.com/o/oauth2/token',
    access_token_params=None,
    authorize_url='https://accounts.google.com/o/oauth2/auth',
    authorize_params=None,
    api_base_url='https://www.googleapis.com/oauth2/v1/',
    jwks_uri='https://www.googleapis.com/oauth2/v3/certs',
    # userinfo_endpoint='https://openidconnect.googleapis.com/v1/userinfo',  # This is only needed if using openId to fetch user info
    client_kwargs={'scope': 'openid email profile'},
)


from app import models
from app import routes
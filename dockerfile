# FROM python:3.8-alpine
# RUN echo "@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
# RUN apk add --update --no-cache py3-numpy py3-pandas@testing
# ENV PYTHONPATH=/usr/lib/python3.8/site-packages

# COPY . /app
# WORKDIR /app


# WORKDIR /app

# COPY requirements.txt requirements.txt


# RUN apk update && apk add --no-cache mysql-dev


# RUN apk add --no-cache --virtual .build-deps g++ python3-dev libffi-dev openssl-dev 
# RUN apk add --no-cache --update python3
# RUN pip3 install --upgrade pip setuptools


# RUN python3 -m venv venv


# RUN pip3 install --upgrade pip
# RUN pip3 install --upgrade wheel
# RUN pip3 install --upgrade setuptools

# RUN pip3 install -r requirements.txt



# RUN pip3 install flask-mysql
# # numpy==1.24.1
# # pandas==1.1.3
# RUN pip3 install pandas







# COPY . .


# ENV FLASK_APP=run.py

# ENV FLASK_RUN_HOST=0.0.0.0

# EXPOSE 80

# CMD ["flask", "run"]


FROM python:3.8

COPY . /app
WORKDIR /app

RUN python3 -m venv venv


RUN apt-get update && \
    apt-get install -y \
    python3-dev \
    python3-pip \
    libatlas-base-dev \
    libblas-dev \
    liblapack-dev \
    gfortran
RUN pip3 install numpy
RUN pip3 install pandas


RUN pip3 install --upgrade pip
RUN pip3 install --upgrade wheel
RUN pip3 install --upgrade setuptools


RUN pip install -r requirements.txt

EXPOSE 80 
ENTRYPOINT [ "python" ] 
CMD [ "run.py" ]
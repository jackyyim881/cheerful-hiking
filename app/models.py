from app import app , db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin
from wtforms import PasswordField
from wtforms.validators import Length


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"



class User(db.Model, UserMixin):
    id = db.Column(db.Integer(),primary_key=True)
    username = db.Column(db.String(length=30), nullable=False, unique=True)
    email_address = db.Column(db.String(length=50),nullable=False, unique=True)
    password1 = PasswordField(label='Password:', validators=[Length(min=6)])
    password_hash = db.Column(db.String(length=60), nullable=False)
    budget = db.Column(db.Integer(),nullable=False, default=1000)
    items = db.relationship('Item', backref='owned_user', lazy=True)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

class Item(db.Model):
    id = db.Column(db.Integer(),primary_key=True)
    name = db.Column(db.String(length=30),nullable=False, unique=True)
    price = db.Column(db.Integer(),nullable=False)
    barcode = db.Column(db.String(length=12), nullable=False,unique=True)
    description = db.Column(db.String(length=1024),nullable=False,unique=True)
    owner = db.Column(db.Integer(),db.ForeignKey('user.id'))

class weatherFLWTC(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    generalSituation = db.Column(db.String(length=200), nullable=False)
    tcInfo = db.Column(db.String(length=200), nullable=True)
    fireDangerWarning = db.Column(db.String(length=200), nullable=True)
    forecastPeriod = db.Column(db.String(length=200), nullable=False)
    forecastDesc = db.Column(db.String(length=1024), nullable=False)
    outlook = db.Column(db.String(length=1024), nullable=False)
    updateTime = db.Column(db.String(length=40), nullable=False)

class weatherFNDTC(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    weatherForecast = db.Column(db.String(length=200), nullable=True)
    forecastDate = db.Column(db.String(length=200), nullable=True)
    forecastWeather = db.Column(db.String(length=200), nullable=True)
    forecastMaxtemp = db.Column(db.String(length=200), nullable=True)
    forecastMintemp = db.Column(db.String(length=200), nullable=True)
    week = db.Column(db.String(length=200), nullable=True)
    forecastWind = db.Column(db.String(length=200), nullable=True)
    forecastMaxrh = db.Column(db.String(length=200), nullable=True)
    forecastMinrh = db.Column(db.String(length=200), nullable=True)
    ForecastIcon = db.Column(db.String(length=200), nullable=True)
    PSR = db.Column(db.String(length=200), nullable=True)
    soilTemp = db.Column(db.String(length=200), nullable=True)
    seaTemp = db.Column(db.String(length=200), nullable=True)

class weatherRHRREADTC(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    lightning = db.Column(db.String(length=200), nullable=True)
    rainfall = db.Column(db.String(length=200), nullable=True)
    icon = db.Column(db.String(length=200), nullable=True)
    iconUpdateTime = db.Column(db.String(length=200), nullable=True)
    uvindex = db.Column(db.String(length=200), nullable=True)
    updateTime = db.Column(db.String(length=200), nullable=True)
    warningMessage = db.Column(db.String(length=200), nullable=True)
    rainstormReminder = db.Column(db.String(length=200), nullable=True)
    specialWxTips = db.Column(db.String(length=200), nullable=True)
    tcmessage = db.Column(db.String(length=200), nullable=True)
    mintempFrom00To00 = db.Column(db.String(length=200), nullable=True)
    rainfallFrom00To12 = db.Column(db.String(length=200), nullable=True)
    rainfallLastMonth = db.Column(db.String(length=200), nullable=True)
    rainfallJanuaryToLastMonth = db.Column(db.String(length=200), nullable=True)
    temperature = db.Column(db.String(length=200), nullable=True)
    humidity = db.Column(db.String(length=200), nullable=True)

class weatherWARNSUMTC(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(length=200), nullable=True)
    code = db.Column(db.String(length=200), nullable=True)
    actionCode = db.Column(db.String(length=200), nullable=True)
    issueTime = db.Column(db.String(length=200), nullable=True)
    expireTime = db.Column(db.String(length=200), nullable=True)
    updateTime = db.Column(db.String(length=200), nullable=True)

class weatherWarningInfoTC(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    details = db.Column(db.String(length=200), nullable=True)
    contents = db.Column(db.String(length=200), nullable=True)
    warningStatementCode = db.Column(db.String(length=200), nullable=True)
    subtype = db.Column(db.String(length=200), nullable=True)
    updateTime = db.Column(db.String(length=200), nullable=True)

class weatherSMTTC(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    desc = db.Column(db.String(length=200), nullable=True)
    updateTime = db.Column(db.String(length=200), nullable=True)

class earthquakeQEMTC(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    lat = db.Column(db.String(length=200), nullable=True)
    lon = db.Column(db.String(length=200), nullable=True)
    mag = db.Column(db.String(length=200), nullable=True)
    region = db.Column(db.String(length=200), nullable=True)
    ptime = db.Column(db.String(length=200), nullable=True)
    updateTime = db.Column(db.String(length=200), nullable=True)

class feltearthquakeTC(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    updateTime = db.Column(db.String(length=200), nullable=True)
    mag = db.Column(db.String(length=200), nullable=True)
    region = db.Column(db.String(length=200), nullable=True)
    lat = db.Column(db.String(length=200), nullable=True)
    lon = db.Column(db.String(length=200), nullable=True)
    details = db.Column(db.String(length=200), nullable=True)
    ptime = db.Column(db.String(length=200), nullable=True)

with app.app_context():
    db.create_all()
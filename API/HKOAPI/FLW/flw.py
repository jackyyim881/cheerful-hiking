import requests


api_url = "https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=flw&lang=tc"
response = requests.get(api_url)
r = response
list_of_dicts = r.json()

def stats():
    if r.status_code == requests.codes.ok:
        return 200

### print(stats())

def GS():

    GS = list_of_dicts['generalSituation']
    return GS

def TI():

    TI = list_of_dicts['tcInfo']
    return TI

def FDW():

    FDW = list_of_dicts['fireDangerWarning']
    return FDW

def FP():

    FP = list_of_dicts['forecastPeriod']
    return FP

def FD():

    FD = list_of_dicts['forecastDesc']
    return FD

def O():

    O = list_of_dicts['outlook']
    return O

def UT():

    UT = list_of_dicts['updateTime']
    return UT